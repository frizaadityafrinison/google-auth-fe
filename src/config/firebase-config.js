import { initializeApp } from 'firebase/app'
import { getAuth, signInWithEmailAndPassword, signOut,signInWithPopup, GoogleAuthProvider } from 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyC6RuGQyskwPx-vI9MiGFH6kSoHYP5AN4A",
  authDomain: "kelompok-3-d7406.firebaseapp.com",
  projectId: "kelompok-3-d7406",
  storageBucket: "kelompok-3-d7406.appspot.com",
  messagingSenderId: "21487527614",
  appId: "1:21487527614:web:35983e766ae04ccd9de0ef"
}

const app = initializeApp(firebaseConfig)
const auth = getAuth(app)
const googleAuth = new GoogleAuthProvider()

export {auth, signInWithEmailAndPassword, signOut, googleAuth, signInWithPopup}