import { useState, useEffect } from "react";
import axios from "axios";
import { auth, signInWithEmailAndPassword, signInWithPopup, googleAuth } from "../config/firebase-config";
import { useNavigate, Link } from "react-router-dom";
import "../App.css";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate()

  // useEffect(() => {
  //   handleLoginWithGoogle()
  // }, [])

  const handleLoginWithEmail = async (event) => {
    event.preventDefault()
    try {
      const data = await signInWithEmailAndPassword(auth, email, password)
      console.log(data);
      navigate('/infoakun')
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleLoginWithGoogle = async () => {
    const data = await signInWithPopup(auth, googleAuth);

    await axios.post("http://localhost:8000/registrasi", {
      uid: data.user.uid,
      name: data.user.displayName,
      provider: data.providerId
    });
    navigate("/infoakun");
  };

  return (
    <>
      <h1>LOGIN</h1>
      <form action="" onSubmit={handleLoginWithEmail}>
        <input
          type="email"
          name=""
          value={email}
          onChange={(event) => setEmail(event.target.value)}
          placeholder="Your Email"
        />
        <input
          type="password"
          name=""
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          placeholder="Your Password"
        />
        <button type="submit">Login</button>
      </form>
      <button onClick={handleLoginWithGoogle}>LOGIN WITH GOOGLE</button>
      <p>Belum punya akun? <Link to='/registrasi'>Daftar</Link></p>
    </>
  );
}

export default Login;
