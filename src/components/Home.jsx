import { auth, signOut } from "../config/firebase-config";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

function Home({ token }) {
  const navigate = useNavigate();
  const [userData, setUserData] = useState()

  useEffect(() => {
    if (token) {
      fetchData(token);
    }
  }, [token]);

  const fetchData = async (token) => {
    const resData = await axios.get("http://localhost:8000/infoakun", {
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    setUserData(resData.data)
  };

  const logout = async () => {
    await signOut(auth);
    navigate("/login");
  };

  const handleUpdate = (event) => {
    event.preventDefault()
  }

  return (
    <>
      <h1>your data (Look at the Console)</h1>
      {JSON.stringify(userData)}
      <form action="" onClick={handleUpdate}>
        <tr>
          <td><input type="text" name="" id=""  /></td>
        </tr>
        <tr>
          <td><input type="text" name="" id="" /></td>
        </tr>
        <tr>
          <td><input type="text" name="" id="" /></td>
        </tr>
        <tr>
          <td><input type="text" name="" id="" /></td>
        </tr>
        <button type="submit">UPDATE!!</button>
      </form>
      <span onClick={logout}>Logout</span>
    </>
  );
}

export default Home;
