import {
  auth,
  signInWithPopup,
  googleAuth,
} from "../config/firebase-config";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

function Registrasi() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleRegister = async (event) => {
    event.preventDefault();
    try {
      await axios.post("http://localhost:8000/registrasi", {
        name: name,
        email: email,
        password: password,
      });
      navigate("/login");
    } catch (err) {
      console.log(err);
    }
  };

  const registerWithGoogle = async () => {
    const data = await signInWithPopup(auth, googleAuth);

    await axios.post("http://localhost:8000/registrasi", {
      uid: data.user.uid,
      name: data.user.displayName,
      provider: data.providerId
    });

    navigate("/infoakun");
  };

  return (
    <>
      <h1>Registrasi</h1>
      <form action="submit" onSubmit={handleRegister}>
        <input
          type="text"
          name=""
          value={name}
          placeholder="Your Name"
          onChange={(e) => setName(e.target.value)}
        />
        <input
          type="email"
          name=""
          value={email}
          placeholder="Your Email"
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          type="password"
          name=""
          value={password}
          placeholder="Your Password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <button type="submit">Register</button>
      </form>
      <button onClick={registerWithGoogle}>REGISTER WITH GOOGLE</button>
      <p>
        Sudah Punya Akun? <Link to={"/login"}>Masuk</Link>
      </p>
    </>
  );
}

export default Registrasi;
