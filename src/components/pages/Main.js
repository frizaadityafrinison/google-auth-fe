import React from "react";
import NavbarMenu from "../Navbar";



const Main = ({
    compBanner,
    compCater,
}) => {
    return (
        <div>
            <NavbarMenu/>
            {compBanner}
            {compCater}
        </div>
    );
};

export default Main;