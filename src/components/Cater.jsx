import React from "react";

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

const Cater = () => {
  return (
    <div>
      <div className="container grid-service grid-service1">
        <div className="row">
          <div className="col-sm">
            <br />
            <p style={{ marginTop: "30px" }}>
              <b>Telusuri Kategori</b>
            </p>
          </div>
        </div>
        <div>
          <button type="button" className="btn kategori active" style={{ marginRight:"10px" }}>
            <img src="img/search1.png" className="me-2" />
            Masuk
          </button>
          <button type="button" className="btn kategori" style={{ marginRight:"10px" }}>
            <img src="img/search1.png" className="me-2" />
            Hobi
          </button>
          <button type="button" className="btn kategori" style={{ marginRight:"10px" }}>
            <img src="img/search1.png" className="me-2" />
            Kendaraan
          </button>
          <button type="button" className="btn kategori" style={{ marginRight:"10px" }}>
            <img src="img/search1.png" className="me-2" />
            Baju
          </button>
          <button type="button" className="btn kategori" style={{ marginRight:"10px" }}>
            <img src="img/search1.png" className="me-2" />
            Elektronik
          </button>
          <button type="button" className="btn kategori" style={{ marginRight:"10px" }}>
            <img src="img/search1.png" className="me-2" />
            Kesehatan
          </button>
        </div>
      </div>
    </div>
  );
};

export default Cater;
