import { Link } from "react-router-dom";
import { Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, FormGroup, Label, Input } from "reactstrap";

const NavbarMenu = () => {
  return (
    <header>
    <div>
      <Navbar color="" expand="md" light container header>
        <NavbarBrand href="/">
          <img src="img/icon.png" style={{ width: "100px" }} className="icon"/>
        </NavbarBrand>
        <NavbarToggler style={{ marginRight:"30px" }} onClick={function noRefCheck() {}} />
              <FormGroup>
                <Input
                  id="exampleSearch"
                  name="search"
                  placeholder="Cari di sini ..."
                  type="search"
                  className="search"
                  style={{ marginTop: "15px" }}
                />
              </FormGroup>
        <Collapse navbar>
          <Nav className="ms-auto" navbar-nav>
            <ul className="navbar-nav ms-auto">
              <li className="nav-item">
              <Link to="/registrasi">
                <button type="button" className="btn tombol">
                  <img src="img/masuk.png" className="me-2"/>
                  Masuk
                </button>
              </Link>
              </li>
            </ul>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
    </header>
  );
};

export default NavbarMenu;
