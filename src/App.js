import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "./App.css";
import { Routes, Route } from "react-router-dom";
import { auth } from "./config/firebase-config";

import Main from "./components/pages/Main";
import Banner from "./components/Banner";
import Cater from "./components/Cater";
import Registrasi from "./components/Registrasi";
import Login from "./components/Login";
import Home from "./components/Home";

function App() {
  const [token, setToken] = useState('')
  const [userData, setUserData] = useState('')

  useEffect(() => {
    auth.onAuthStateChanged((data) => {
      console.log(data)
      data.getIdToken().then((token) => {
        setToken(token)
      })
    })
  });

  return (
    <div className="App">
      <Routes>
        <Route
          path="/"
          element={<Main compBanner={<Banner />} compCater={<Cater />} />}
        />
        <Route path="/registrasi" element={<Registrasi />} />
        <Route path="/login" element={<Login />} />
        <Route path="/infoakun" element={<Home token={token} />} />
      </Routes>
    </div>
  );
}

export default App;
